import { Request, Response } from 'express';
import { connect } from '../database';
import { Post } from '../interface/post';

//Regresa un post segun parametro
export async function getPost(req:Request, res:Response):Promise<Response>{
	const conn = await connect();
	const id = req.params.postId;
	const result = await conn.query('SELECT * FROM posts WHERE id = ?',[id]);
	return res.json(result[0]);
}

//Regresa todos los posts
export async function getPosts(req:Request, res:Response):Promise<Response>{
	const conn = await connect();
	const result = await conn.query('SELECT * FROM posts');
	return res.json(result[0]);
}

//Elimina un post segun parametro
export async function deletePost(req:Request, res:Response){
	const conn = await connect();
	const id = req.params.postId;
	await conn.query('DELETE FROM posts WHERE id = ?',[id]);
	return res.json({	message: 'Post Deleted'		});
}




//***** Falta Acomodar

//Modifica un post segun parametro *********************
export async function updatePost(req:Request, res:Response){
	const id = req.params.postId;
	const updatePost: Post = req.body;
	console.log('chao',updatePost);
	const conn = await connect();
	//await conn.query('UPDATE posts set ? WHERE id = ?',[updatePost,id]);
	return res.json({	message: 'Post Updated'		});
}

//Crea un post *********************
export async function createPost(req:Request, res:Response){
	const newPost: Post = req.body;
	console.log('hola',newPost);
	const conn = await connect();
	//await conn.query('INSERT INTO posts SET ?',[newPost]);
	return res.json({	message: 'Post Created'		});
}